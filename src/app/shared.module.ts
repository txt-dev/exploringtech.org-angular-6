import { NgModule } from '@angular/core';
import { FormFieldErrorComponent } from './_util/form-field-error/form-field-error.component';
import { FormFieldSuccessComponent } from './_util/form-field-success/form-field-success.component';
import { CommonModule } from '../../node_modules/@angular/common';

@NgModule({
  declarations: [
    FormFieldErrorComponent,
    FormFieldSuccessComponent
  ],
  imports: [
      CommonModule
  ],
  exports: [
    FormFieldErrorComponent,
    FormFieldSuccessComponent
  ]
})
export class SharedModule {

}
