import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [
    HomeComponent,
    TestimonialsComponent,
  ],
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forChild([
      { path: '', component: HomeComponent, pathMatch: 'full' }
    ])
  ]
})
export class HomeModule {

}
