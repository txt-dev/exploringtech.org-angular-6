import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public laTimes = 'assets/pages/home/sponsors/laTimes.png';
  public nbc = 'assets/pages/home/sponsors/nbc.png';
  public fox = 'assets/pages/home/sponsors/fox.png';
  public sxsw = 'assets/pages/home/sponsors/sxsw.png';
  public sxs = 'assets/pages/home/sponsors/sxs.png';
  public tedx = 'assets/pages/home/sponsors/tedx.png';
  public univision = 'assets/pages/home/sponsors/univision.png';
  public whiteHouse = 'assets/pages/home/sponsors/whiteHouse.png';
  
  constructor() { }

  ngOnInit() {
  }

}
