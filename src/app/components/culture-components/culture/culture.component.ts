import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-culture',
  templateUrl: './culture.component.html',
  styleUrls: ['./culture.component.css']
})
export class CultureComponent implements OnInit {
  public banner = 'assets/pages/culture/creed.jpg';
  public pillars;

  constructor() { }

  ngOnInit() {
    this.initializePillars();
  }

  initializePillars(){
    this.pillars = [
      {
        title: 'Academics',
        desc: 'In SCLA, you will learn to code using HTML, CSS, JavaScript and Python. You will learn how to build your own startup tech business. At SCLA you will learn how to learn. You will master your strength to become the best version of yourself. After a few weeks with us you will understand that success is not hard just a lot of work, but you will thrive. We encourage you to become more than a coder: become a leader and entrepreneur, someone who can overcome any obstacle in his life. Last year, a 14-year-old received $4,000 to start his own company. This year, it can be you.',
        img: 'assets/pages/home/approach/design-thinking.svg'
      },
      {
        title: 'Leadership',
        desc: 'At the University of Southern California, you will be joining a network of the best of the best innovators. You will be joining the “Navy Seals” of tech in Los Angeles. The experience will make you competitive when applying to universities and future jobs. SCLA will give you the opportunity to visit companies such as Google, Yahoo, and Snapchat to learn how they innovate. You will make connections with tech professionals that can become your mentors and help you become successful in the tech world.',
        img: 'assets/pages/home/approach/full-stack.svg'
      },
      {
        title: 'Community',
        desc: 'During 12 weeks, you will become part of a brotherhood of leaders, a brotherhood that inspires you to give it your all. Your brothers will help you code, solve problems, and succeed in life. You will be part of leaders who have changed their communities and families. Today we have TXT’rs from Los Angeles to New York, working to change the world. To us, it doesn\'t matter if you have bad grades or good grades. What matters is that you are hungry to be the best. You will become the best public speaker, the most critical-thinker while you build confidence and become a young professional. You will meet other like-minded individuals who are changing the world and making an impact in their community. We invite you to become one of our leaders. Join our brotherhood to change the world.',
        img: 'assets/pages/home/approach/business.svg'
      },
      {
        title: 'Technology',
        desc: 'At the University of Southern California, you will be joining a network of the best of the best innovators. You will be joining the “Navy Seals” of tech in Los Angeles. The experience will make you competitive when applying to universities and future jobs. SCLA will give you the opportunity to visit companies such as Google, Yahoo, and Snapchat to learn how they innovate. You will make connections with tech professionals that can become your mentors and help you become successful in the tech world.',
        img: 'assets/pages/home/approach/full-stack.svg'
      }
    ]
  } 

}
