import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CultureComponent } from './culture/culture.component';


@NgModule({
  declarations: [
    CultureComponent
  ],
  imports: [
  CommonModule,
  RouterModule.forChild([
      { path: '', component: CultureComponent, pathMatch: 'full' },
    ])
  ]
})
export class CultureModule {

}
