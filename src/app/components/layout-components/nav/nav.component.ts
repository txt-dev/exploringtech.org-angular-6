import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuService } from '../../../services/menu.service';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { isFormattedError } from '@angular/compiler';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  public activeNavMenu = false;
  public getInvovledActive = false;

  constructor(public _menuService: MenuService, public _authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  toggleNavMenu(){
    this.activeNavMenu = !this.activeNavMenu;
  }

  toggleGetInvovled(){
    this.getInvovledActive = !this.getInvovledActive;
  }


  navigate(id){
    console.log('navigate');
    this.router.navigate(['get-involved', id])
  }
}
