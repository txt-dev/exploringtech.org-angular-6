import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    ContactUsComponent,
    ContactFormComponent,

  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ContactUsComponent, pathMatch: 'full' },
    ])
  ]
})
export class ContactUsModule {

}
