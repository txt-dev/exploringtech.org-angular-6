import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../../services/form.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  public otherOptions;
  public showOther = false;
  public loading = false;
  public serverErrorResponse = false;
  public serverSuccessResponse = false;

  constructor(public fb: FormBuilder, public formService: FormService) {

  }

  ngOnInit() {
    this.otherOptions = [
      'General Inquiry',
      'How can I get involved',
      'Joining your program',
      'Other'
    ]
    this.contactForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      reason: ['', [Validators.required]],
      otherReason: [''],
      comments: [''],
    })
  }

  onSubmit() {
    this.serverErrorResponse = false;
    this.serverSuccessResponse = false;

    if (this.contactForm.valid) {
      this.loading = true;
      console.log(this.contactForm.value);

      this.formService.submitContactForm(this.contactForm.value)
        .subscribe((res) => {
          this.loading = false;
          this.serverSuccessResponse = true;
          this.contactForm.reset();
          console.log(res);
        }, (err) => {
          this.loading = false;
          this.serverErrorResponse = true;
          console.log(err);
        })

    }
  }

  selectChange(e) {

    if (e === 'Other') {
      this.showOther = true;
    } else {
      this.showOther = false;
    }
  }

  get name() { return this.contactForm.get('name'); }

  get email() { return this.contactForm.get('email'); }

  get phoneNumber() { return this.contactForm.get('phoneNumber'); }

  get reason() { return this.contactForm.get('reason'); }


}
