import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../../../services/form.service';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.scss']
})
export class MentorComponent implements OnInit {
  mentorForm: FormGroup;
  public menteeMeetings;
  public menteeLength;
  public menteeNum;
  public showOther = false;
  public loading = false;
  public serverErrorResponse = false;
  public serverSuccessResponse = false;

  constructor(public fb: FormBuilder, public formService: FormService) {

  }

  ngOnInit() {
    this.menteeMeetings = [
      'Yes, I can meet with mentee once a month and maintain weekly communication.',
      'No, I have other commitments and can only mentor remotely',
      'I can meet with mentee more than two times a month and maintain weekly communication',
      'I am not sure what I can commit to, but would still like to get involved'
    ]

    this.menteeLength = [
      '6 months',
      '12 months',
      '18 months',
      'Other',
    ]

    this.menteeNum = [
      '1 student',
      '2 students',
      '3 students'
    ]
    this.mentorForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      company: ['', [Validators.required]],
      position: ['', [Validators.required]],
      reference: ['', [Validators.required]],
      volunteerExperience: [''],
      mentorMenteeMeetings: ['', [Validators.required]],
      mentorMenteeLength: ['', [Validators.required]],
      mentorMenteeNum: ['', [Validators.required]],
      interest: ['',],

    })
  }

  onSubmit() {
    this.serverErrorResponse = false;
    this.serverSuccessResponse = false;

    if (this.mentorForm.valid) {
      this.loading = true;
      console.log(this.mentorForm.value);

      this.formService.submitMentorForm(this.mentorForm.value)
        .subscribe((res) => {
          this.loading = false;
          this.serverSuccessResponse = true;
          this.mentorForm.reset();
          console.log(res);
        }, (err) => {
          this.loading = false;
          this.serverErrorResponse = true;
          console.log(err);
        })

    }
  }

  selectChange(e) {

    if (e === 'Other') {
      this.showOther = true;
    } else {
      this.showOther = false;
    }
  }

  get name() { return this.mentorForm.get('name'); }

  get email() { return this.mentorForm.get('email'); }

  get phoneNumber() { return this.mentorForm.get('phoneNumber'); }

  get company() { return this.mentorForm.get('company'); }

  get position() { return this.mentorForm.get('position'); }

  get reference() { return this.mentorForm.get('reference'); }

  get mentorMenteeMeetings() { return this.mentorForm.get('mentorMenteeMeetings'); }

  get mentorMenteeLength() { return this.mentorForm.get('mentorMenteeLength'); }

  get mentorMenteeNum() { return this.mentorForm.get('mentorMenteeNum'); }

  


}
