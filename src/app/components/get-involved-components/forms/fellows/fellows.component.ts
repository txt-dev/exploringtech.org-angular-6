import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../../../services/form.service';

@Component({
  selector: 'app-fellows',
  templateUrl: './fellows.component.html',
  styleUrls: ['./fellows.component.scss']
})
export class FellowsComponent implements OnInit {
  fellowsForm: FormGroup;
  public loading = false;
  public serverErrorResponse = false;
  public serverSuccessResponse = false;

  constructor(public fb: FormBuilder, public formService: FormService) {

  }

  ngOnInit() {

    this.fellowsForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      inspiration: ['', [Validators.required]],
      idealCandidate: ['', [Validators.required]],
    })
  }

  onSubmit() {
    this.serverErrorResponse = false;
    this.serverSuccessResponse = false;

    if (this.fellowsForm.valid) {
      this.loading = true;
      console.log(this.fellowsForm.value);

      this.formService.submitFellowsForm(this.fellowsForm.value)
        .subscribe((res) => {
          this.loading = false;
          this.serverSuccessResponse = true;
          this.fellowsForm.reset();
          console.log(res);
        }, (err) => {
          this.loading = false;
          this.serverErrorResponse = true;
          console.log(err);
        })

    }
  }

  selectChange(e) {
  }

  get name() { return this.fellowsForm.get('name'); }

  get email() { return this.fellowsForm.get('email'); }

  get phoneNumber() { return this.fellowsForm.get('phoneNumber'); }

  get inspiration() { return this.fellowsForm.get('inspiration'); }

  get idealCandidate() { return this.fellowsForm.get('idealCandidate'); }

}
