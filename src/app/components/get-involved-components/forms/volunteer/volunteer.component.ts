import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../../../services/form.service';

@Component({
  selector: 'app-volunteer',
  templateUrl: './volunteer.component.html',
  styleUrls: ['./volunteer.component.scss']
})
export class VolunteerComponent implements OnInit {
  volunteerForm: FormGroup;
  public helpOptions;
  public showOther = false;
  public loading = false;
  public serverErrorResponse = false;
  public serverSuccessResponse = false;

  constructor(public fb: FormBuilder, public formService: FormService) {

  }

  ngOnInit() {
    this.helpOptions = [
      'Mission 42',
      'Workshops',
      'Fundraising',
      'Program Administration',
      'Hackathon',
      'Other'
    ]
    this.volunteerForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      helpOption: ['', [Validators.required]],
      otherHelpOption: [''],
      experience: [''],
      hearAbout: [''],
      inspiration: [''],
      questions: [''],
    })
  }

  onSubmit() {
    this.serverErrorResponse = false;
    this.serverSuccessResponse = false;

    if (this.volunteerForm.valid) {
      this.loading = true;
      console.log(this.volunteerForm.value);

      this.formService.submitVolunteerForm(this.volunteerForm.value)
        .subscribe((res) => {
          this.loading = false;
          this.serverSuccessResponse = true;
          this.volunteerForm.reset();
          console.log(res);
        }, (err) => {
          this.loading = false;
          this.serverErrorResponse = true;
          console.log(err);
        })

    }
  }

  selectChange(e) {

    if (e === 'Other') {
      this.showOther = true;
    } else {
      this.showOther = false;
    }
  }

  get name() { return this.volunteerForm.get('name'); }

  get email() { return this.volunteerForm.get('email'); }

  get phoneNumber() { return this.volunteerForm.get('phoneNumber'); }


}
