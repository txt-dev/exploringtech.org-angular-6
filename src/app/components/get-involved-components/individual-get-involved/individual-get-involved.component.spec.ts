import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualGetInvolvedComponent } from './individual-get-involved.component';

describe('IndividualGetInvolvedComponent', () => {
  let component: IndividualGetInvolvedComponent;
  let fixture: ComponentFixture<IndividualGetInvolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualGetInvolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualGetInvolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
