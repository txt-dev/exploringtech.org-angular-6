import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
declare var gtag: Function;
@Component({
  selector: 'app-individual-get-involved',
  templateUrl: './individual-get-involved.component.html',
  styleUrls: ['./individual-get-involved.component.scss']
})
export class IndividualGetInvolvedComponent implements OnInit {
  public items = [];
  public currentItem;
  public sub;
  constructor(public activatedRoute: ActivatedRoute, public router: Router, public zone: NgZone) {
  }

  ngOnInit() {
    console.log('init');
    this.initializePrograms();
  }

  // populate programs array with all programs
  initializePrograms() {
    this.items = [
      {
        img: 'assets/pages/get-involved/volunteer.jpg',
        name: 'Join as a volunteer',
        bannerMessage: 'Submit this volunteering form so that we can further discuss ways that you can help us. Remember, you can always reach us at',
        bannerEmailer: 'info@urbantxt.com',
        sections: [
          {
            title: '',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'As a non-profit, volunteered help is an integral part of what makes this organization successful. Aside from mentoring opportunities, TXT has benefitted from volunteers who help with program administration, organizing and teaching workshops, setting up field trips for the teens, marketing, fundraising, and many other ways. You can be a part of transforming the future of the South LA tech scene, and empowering youth of color.'
          },
        ],
        form: 'volunteer',
        id: 'volunteer'
      },
      {
        img: 'assets/pages/get-involved/mentor.jpg',
        name: 'Join as a mentor',
        bannerMessage: 'Submit this mentoring form so that we can further discuss ways that you can help us. Remember, you can always reach us at',
        bannerEmailer: 'info@urbantxt.com',
        sections: [
          {
            title: '',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'Are you a passionate professional in the technology industry? You can make a difference in a teen\'s life by contributing your time and expertise as part of our weekly workshops. Contact us and let us know how you can help.'
          },
        ],
        form: 'mentor',
        id: 'mentor'
      },

      {
        img: 'assets/pages/get-involved/fellows.jpg',
        name: 'Become a TXT fellow',
        bannerMessage: 'Submit this fellowship form so that we can further discuss ways that you can help us. Remember, you can always reach us at',
        bannerEmailer: 'info@urbantxt.com',
        sections: [
          {
            title: '',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'As a TXT fellow, you will grow as an individual while inspiring the next generation of tech entrepreneurs to develop solutions for social problems in low-income communities. Through 3 months, you will develop skills that will advance your career in leadership, entrepreneurship, and professionalism by managing teen startups through coding, design thinking and the lean startup methodology.'
          },
        ],
        form: 'fellow',
        id: 'fellow'
      },

      {
        img: 'assets/pages/contact/banner.jpg',
        name: 'Join the kick ass team',
        bannerMessage: '',
        bannerEmailer: '',
        sections: [
          {
            title: '',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'TXT: Teens Exploring Technology seeks to hire individuals who will contribute to our larger goal of providing high quality STEM education for black and latino boys. '
          },
        ],
        form: 'jobs',
        id: 'jobs'
      },


    ]

    this.sub = this.activatedRoute.params.subscribe((res) => {
      this.currentItem = false;
      this.currentItem = this.items.filter((data) => {
        if (data.id === res.id) {
          return data;
        }
      })
      this.currentItem = this.currentItem[0];

      if (!this.currentItem) {
        this.router.navigate(['/get-involved'])
      }
    });
  }

  // unsubscribe to avoid memory leaks
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
