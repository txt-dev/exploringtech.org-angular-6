import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-involved',
  templateUrl: './get-involved.component.html',
  styleUrls: ['./get-involved.component.css']
})
export class GetInvolvedComponent implements OnInit {
  public banner = 'assets/pages/get-involved/banner.jpg';
  public volunteer = 'assets/pages/get-involved/volunteer.jpg';
  public mentor = 'assets/pages/get-involved/mentor.jpg';
  public fellow = 'assets/pages/get-involved/fellows.jpg';
  public donate = 'assets/pages/get-involved/donate.JPG';
  public items;

  constructor(public router: Router) { }

  ngOnInit() {
    this.initializeItems();
  }


  initializeItems(){
    this.items = [
      {
        img: 'assets/pages/get-involved/volunteer.jpg',
        name: 'Become a volunteer',
        desc: 'Just because you do not know design or programming it does not mean you can not help us organize events, run a workshop or coordinate meetings and more.',
        id: 'volunteer',
        externalLink: false,
        link: ''
      },
      {
        img: 'assets/pages/get-involved/mentor.jpg',
        name: 'Become a mentor',
        desc: 'You can make a difference in a teen\'s life by contributing your time and expertise as part of our weekly workshops. Contact us and let us know how you can help.',
        id: 'mentor',
        externalLink: false,
        link: ''
      },
      {
        img: 'assets/pages/get-involved/fellows.jpg',
        name: 'Become a fellow',
        desc: 'As a TXT fellow, you will grow as an individual while inspiring the next generation of tech entrepreneurs to develop solutions for social problems in low-income communities.',
        id: 'fellow',
        externalLink: false,
        link: ''
      },
      {
        img: 'assets/pages/get-involved/donate.JPG',
        name: 'Help us by donating',
        desc: 'We are a non-profit organization and every little bit helps. We have made it easy for your to donate to our cause. Please help us keep our program alive by donating any amount.',
        id: 'donate',
        externalLink: true,
        link: 'https://txt.networkforgood.com'
      }
    ]
  }

  viewItem(id){
    this.router.navigate(['/get-involved', id])
  }

}
