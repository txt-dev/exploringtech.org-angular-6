import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GetInvolvedComponent } from './get-involved/get-involved.component';
import { IndividualGetInvolvedComponent } from './individual-get-involved/individual-get-involved.component';
import { FellowsComponent } from './forms/fellows/fellows.component';
import { MentorComponent } from './forms/mentor/mentor.component';
import { VolunteerComponent } from './forms/volunteer/volunteer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';
import { JobsComponent } from './forms/jobs/jobs.component';
@NgModule({
  declarations: [
    GetInvolvedComponent,
    IndividualGetInvolvedComponent,
    FellowsComponent,
    MentorComponent,
    VolunteerComponent,
    JobsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: GetInvolvedComponent, pathMatch: 'full' },
      { path: ':id', component: IndividualGetInvolvedComponent, pathMatch: 'full' },
    ])
  ]
})
export class GetInvolvedModule {

}
