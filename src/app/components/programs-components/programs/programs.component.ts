import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.css']
})
export class ProgramsComponent implements OnInit {
  public banner = 'assets/pages/programs/banner.jpg';
  public jamesIrvineVideo = 'https://www.youtube.com/embed/6gy5G0JpTQ8';
  public scla = 'assets/pages/programs/scla/scla-1.jpg';
  public seals = 'assets/pages/programs/seals/seals.jpg';
  public fostercode = 'assets/pages/programs/fostercode/fostercode.JPG';
  public hackathon = 'assets/pages/programs/hackathon/hackathon.jpeg';
  public labs = 'assets/pages/programs/labs/banner.png';

  public programs = []
  constructor(public sanitizer: DomSanitizer, public router: Router) { }

  ngOnInit() {
    this.initializePrograms();
  }
  // populate programs array with all programs
  initializePrograms() {
    this.programs = [
      {
        img: 'assets/pages/programs/scla/scla-1.jpg',
        name: 'Summer Coding Leadership Academy (SCLA)',
        desc: 'A 12-week computer science and leadership program for low-income young men of color in 8th-11th grades held at the University of Southern California (USC). SCLA is our flagship program, where our process of developing a generation of tech leaders starts.',
        id: 'scla',
        externalLink: false,
        link: ''
      },
      {
        img: 'assets/pages/programs/seals/seals.jpg',
        name: 'SEALS',
        desc: 'A 10-week, advanced programming curriculum for SCLA graduates.',
        id: 'seals',
        externalLink: false,
        link: ''
      },
      {
        img: 'assets/pages/programs/labs/banner.png',
        name: 'TXT Labs',
        desc: 'TXT Labs is TXT’s social enterprise arm specializing in software development & IT services and focused on providing end-to-end solutions, including ideation, development, and marketing.',
        id: 'scla',
        externalLink: true,
        link: 'http://txtlabs.io/'
      },
      {
        img: 'assets/pages/programs/hackathon/hackathon.jpeg',
        name: 'Hustle N Code',
        desc: 'Every year, TXT hosts a Hackathon at a public housing project in South Central Los Angeles.',
        id: 'hackathon',
        externalLink: false,
        link: ''
      }
    ]
  }

  // navigate to the individual program page
  viewProgram(id){
    this.router.navigate(['/programs', id])
  }
}
