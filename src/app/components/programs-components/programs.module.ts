import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProgramsComponent } from './programs/programs.component';
import { IndividualProgramComponent } from './individual-program/individual-program.component';

@NgModule({
  declarations: [
    ProgramsComponent,
    IndividualProgramComponent
  ],
  imports: [
  CommonModule,
  RouterModule.forChild([
      { path: '', component: ProgramsComponent, pathMatch: 'full' },
      { path: ':id', component: IndividualProgramComponent, pathMatch: 'full' }
    ])
  ]
})
export class ProgramsModule {

}
