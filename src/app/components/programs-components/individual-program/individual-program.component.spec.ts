import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualProgramComponent } from './individual-program.component';

describe('IndividualProgramComponent', () => {
  let component: IndividualProgramComponent;
  let fixture: ComponentFixture<IndividualProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
