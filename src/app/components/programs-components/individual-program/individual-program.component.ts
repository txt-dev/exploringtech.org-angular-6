import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-individual-program',
  templateUrl: './individual-program.component.html',
  styleUrls: ['./individual-program.component.scss']
})
export class IndividualProgramComponent implements OnInit {
  public programs = [];
  public currentProgram;
  constructor(public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.initializePrograms();
  }

  // populate programs array with all programs
  initializePrograms() {
    this.programs = [
      {
        img: 'assets/pages/programs/scla/scla-1.jpg',
        name: 'Summer Coding Leadership Academy (SCLA)',
        desc: 'A 12-week computer science and leadership program for low-income young men of color in 8th-11th grades held at the University of Southern California (USC). SCLA is our flagship program, where our process of developing a generation of tech leaders starts.',
        bannerMessage: 'The application to the Summer Coding Leadership Academy is now closed. Still interested? Send an email to',
        bannerEmailer: 'programs@urbantxt.com',
        programSections: [
          {
            title: 'Learn coding and entrepreneurship skills',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'In SCLA, you will learn to code using HTML, CSS, JavaScript and Python. You will learn how to build your own startup tech business. At SCLA you will learn how to learn. You will master your strength to become the best version of yourself. After a few weeks with us you will understand that success is not hard just a lot of work, but you will thrive. We encourage you to become more than a coder: become a leader and entrepreneur, someone who can overcome any obstacle in his life. Last year, a 14-year-old received $4,000 to start his own company. This year, it can be you.'
          },
          {
            title: 'Innovate with others while you build a network',
            img: 'assets/pages/home/approach/full-stack.svg',
            content: 'At the University of Southern California, you will be joining a network of the best of the best innovators. You will be joining the “Navy Seals” of tech in Los Angeles. The experience will make you competitive when applying to universities and future jobs. SCLA will give you the opportunity to visit companies such as Google, Yahoo, and Snapchat to learn how they innovate. You will make connections with tech professionals that can become your mentors and help you become successful in the tech world.'
          },
          {
            title: 'Lead and become part of a brotherhood',
            img: 'assets/pages/home/approach/business.svg',
            content: 'During 12 weeks, you will become part of a brotherhood of leaders, a brotherhood that inspires you to give it your all. Your brothers will help you code, solve problems, and succeed in life. You will be part of leaders who have changed their communities and families. Today we have TXT’rs from Los Angeles to New York, working to change the world. To us, it doesn\'t matter if you have bad grades or good grades. What matters is that you are hungry to be the best. You will become the best public speaker, the most critical-thinker while you build confidence and become a young professional. You will meet other like-minded individuals who are changing the world and making an impact in their community. We invite you to become one of our leaders. Join our brotherhood to change the world.'
          }
        ],
        id: 'scla'
      },
      {
        img: 'assets/pages/programs/seals/seals.jpg',
        name: 'SEALS',
        bannerMessage: 'This program is only available to SCLA graduates. Interested? Send an email to',
        bannerEmailer: 'programs@urbantxt.com',
        programSections: [
          {
            title: 'Take your skills to the next level',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'TXT’s SEAL program expands on the topics learned during SCLA in order for students to become master full-stack developers. We emphasize deep learning in key concepts such as debugging, algorithms, loops, functions, and databases. By the end of the SEALs program, students are able to develop their own web or mobile application. Students also learn to collaborate on projects using Git. Students who take the SEAL program consistently do extremely well in their first JAVA classes in college.'
          }
        ],
        id: 'seals'
      },
      {
        img: 'assets/pages/programs/hackathon/hackathon.jpeg',
        name: 'Hustle N Code',
        bannerMessage: 'We host our hacakthon every year. For more info Send an email to',
        bannerEmailer: 'kevin@urbantxt.com',
        programSections: [
          {
            title: '',
            img: 'assets/pages/home/approach/design-thinking.svg',
            content: 'TXT’s Hustle N’ Code Hackathon is an event aimed at inspiring youth who live in public housing developments (i.e. “The Projects”) to address pressing issues in their communities using technology. TXT is the only organization in the nation that is hosting hackathons directly in public housing developments, and the need is high: our data has shown that less than 1% of youth in public housing developments in South Central have access to the internet, and less than 5% have access to a computer at home. Thanks to Hustle N’ Code, hundreds of teens from South Central Los Angeles will be exposed to computer science for the first time.'
          },
          {
            title: '',
            img: 'assets/pages/home/approach/full-stack.svg',
            content: 'During our hackathons, boys and girls ages 11-18 form groups of four and are joined by professional developers to solve a community issue with code. The program doesn’t stop there, though: all of our hackathon participants have the opportunity to continue coding and learning, for free, at our Hackerspace, year-round. Our hackathons typically reach over 200 families in South Central Los Angeles. At the end of the day, we hope to inspire underserved tweens and teens to dream about being techies and programmers.'
          }
        ],
        id: 'hackathon'
      }
    ]

    this.activatedRoute.params.subscribe((res) => {
      this.currentProgram = this.programs.filter((data) => {
        if(data.id === res.id){
          return data;
        }
      })
    });

    this.currentProgram = this.currentProgram[0];
  }
}
