import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { TeamComponent } from './team/team.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AboutComponent,
    TeamComponent,

  ],
  imports: [
  CommonModule,
  RouterModule.forChild([
      { path: '', component: AboutComponent, pathMatch: 'full' },
      { path: 'team/:id', component: TeamComponent, pathMatch: 'full' }
    ])
  ]
})
export class AboutModule {

}
