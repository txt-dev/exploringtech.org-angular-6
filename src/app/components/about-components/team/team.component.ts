import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  public team = [
    {
      id: "oscar",
      pic: "assets/pages/about/team/oscar.jpg",
      title: 'Founder and CEO',
      name: "Oscar Menjivar",
      bio: [
        "Oscar Menjivar is a social entrepreneur, leading and developing new programs for low-income communities in Los Angeles. Oscar is a dedicated individual and devotes much of this time to bettering his community. Oscar founded TXT: Teens Exploring Technology, a nonprofit organization that provides a program to help inner-city youth develop leadership, coding, and entrepreneurial skills through the use of technology. ",
        "TXT is revolutionizing the way teens use and learn about technology in the inner city. In 2016, Oscar Menjivar presented at the White House's South By South Lawn event, earning recognition from President Obama. He was also a featured presenter at the 2017 SXSWedu Festival.",
        "As a young man, Oscar was intrigued by computers and decided to attend a magnet high school for its technology program. However, to his surprise, not much technology existed on campus in 1996, and the most effective organizations in recruiting young men were gangs. Oscar saw one of his best friends fall victim to gangs and pledged that he would help create a program that would inspire at-risk youth to become leaders that impact their communities in a positive way. His experiences inspired him to start TXT in 2009.",
        "Oscar received his Master’s degree from Pepperdine in Learning Technology and his Bachelor’s in Science in Computer Information Systems. He formerly served as Educational Technology Consultant for The Riordan Foundation (TRF). At TRF, he advised school executives on how to implement technology in the classroom and developed technology curriculum for Green Dot Animo Pat Brown High School. He also served as a multimedia arts and leadership consultant for the USC Upward Bound Math/Science Program. During his tenure at USC, he successfully implemented a leadership and digital arts course for youth in Los Angeles. The digital arts course produced winners for international and national website competitions.",
        "Oscar has run a successful consulting business for over four years. He worked as a technology consultant for Small Businesses in the Los Angeles County. As a small business consultant, Oscar guided over 100 small business owners with business planning and budgeting. In 2004, Oscar joined the Long Beach YMCA Youth Institute as Multimedia Technology Program Coordinator. Oscar learned how to start and run a successful social enterprise at the Youth Institute.",
      ],
      email: "mailto:oscar@gmail.com"
    },
    {
      id: "roberto",
      pic: "assets/pages/about/team/roberto.jpg",
      title: 'CTO',
      name: "Roberto Sanchez",
      bio: ["Roberto Sanchez leads curriculum development, instruction, and training of USC students. He graduated with a B.A. in Economics from USC in May 2017. A South Central native, Roberto previously taught at-risk youth during TXT’s Summer Coding Leadership Academy, the USC Readers Plus program, USC Hybrid High Summer School and Foshay Learning Center Summer School. He is also a proud graduate of the USC Neighborhood Academic Initiative Program."],
      email: "mailto:roberto@gmail.com"
    },
    {
      id:"john",
      pic: "assets/pages/about/team/oscar.png",
      title: 'Development Manager',
      name: "John Barrios",
      bio: [
        "John Barrios is the manager of development at TXT, where he will manage corporate, foundation, and individual giving. John was born and raised in East Los Angeles, and he is passionate about serving and empowering the community that helped raised him.",
        "John was raised in a low-income home by his mom and grandmother (he calls her mamá), and he attributes all of his successes to having a strong, loving, and empowering network of family and friends. Outside of work, John volunteers with organizations such as Generation First Degree Pico Rivera (G1DPR) and SCS Noonan Scholars.",
        "John studied at Yale University, where he graduated, with distinction, with a BA in Latin American Studies. Prior to joining TXT, John was a manager of development at the Hispanic Scholarship Fund, where he oversaw individual giving and assisted with corporate development. As an undergraduate, he worked in Yale’s Office of Development as a prospect researcher.",
        "In his free time, John enjoys reading about history, exploring the great outdoors, and spending quality time with his loved ones."
      ],
      email: "mailto:john@urbantxt.com"
    },
    {
      id:"roxanne",
      pic: "assets/pages/about/team/roxanne.jpg",
      title: 'Board Member and Co-Founder',
      name: "Roxanne Mendez",
      bio: [
        "Roxanne is Co-Founder and Board Chair of Teens Exploring Technology. Roxanne Mendez serves as the Executive Director of the Riordan Programs. In addition, she is also certified Career Management Coach. Ms. Mendez was selected for the Next Generation Latino Philanthropy Leaders Program by Hispanics in Philanthropy. In 2015, she was a member of Leadership California for its California Issues & Trends Program, a prestigious, year-long program for women leaders from across the state of California. Her previous experience includes working in college outreach at the University of Southern California and as grant consultant, establishing a pre-medical school program for underrepresented populations. Roxanne is a graduate of California State Polytechnic University, Pomona with a B.S. in business administration. She received her graduate degree from the University of Southern California."
      ],
      email: "mailto:roxanne@urbantxt.com"
    },
    {
      id:"danny",
      pic: "assets/pages/about/team/danny.jpg",
      title: 'Executive Assistant',
      name: "Danny Menjivar",
      bio: [
        "Danny is responsible for assisting TXT’s CEO, Oscar Menjivar, including taking meeting minutes and handling day to day administrative operations. Additionally, he is Head of Culture for TXT’s Summer Coding Leadership Academy.",
        "A native of South Central Los Angeles, Danny studies at Cal State LA and is majoring in Electrical Engineering. He was raised with two sisters by a single mother. At a young age, he had to work while he went to school so that he could support his family and be a role model to his sisters.",
        "Growing up, Danny was involved in his community church, where he eventually became a youth leader and Sunday school teacher. It was through these experiences that he discovered his love for working with youth and giving back to his community.  Beyond making a positive impact on the students he works with, Danny is dedicated to improving his community as whole to make his neighborhood a better place to live."
      ],
      email: "mailto:danny@urbantxt.com"
    },
    {
      id:"alex",
      pic: "assets/pages/about/team/alex.jpg",
      title: 'Associate Director of Programs',
      name: "Alex Ruiz",
      bio: [
        "Alex Ruiz serves as the Associate Director of Programs at TXT: Teens Exploring Technology. Born and raised in South Central Los Angeles, Alex was part of the first graduating class at Wallis Annenberg High School and the only one in his class to get accepted into UC Berkeley.",
        "While at Berkeley, Alex worked in various schools from Richmond to Oakland, and he served as the Alumni Chair for Hermanos Unidos. After graduating in 2012 with BA in Sociology, Alex has worked in various capacities within education and has continuously been an advocate for students and their families."
      ],
      email: "mailto:alex@urbantxt.com"
    },
    {
      id:"kevin",
      pic: "assets/pages/about/team/kevin.jpg",
      title: 'Community Engagement Coordinator',
      name: "Kevin Martinez",
      bio: [
        "Kevin Martinez is the Community Engagement Coordinator at TXT. His primary responsibility is to coordinate our annual Hackathon in addition to developing and sustaining authentic partnerships with students, families and other crucial community members. Kevin was born and raised in South Central LA by a single, undocumented mother from Mexico City. He also considers Boyle Heights his second home since he graduated from Roosevelt High School in 2012.",
        "Kevin pursued his Bachelors in Sociology at Westminster College in Salt Lake City (2016) and recently obtained his Masters in Education from the University of Utah (2018). During his six years in Utah, he made sure to balance his academic work with community involvement because he believes that academia and community work should not be independent of one another.",
        "Prior to joining TXT, Kevin worked for the Diversity Scholars program at the University of Utah as a Community Engagement Learning Coordinator where he taught Ethnic Studies and coordinated critical service-learning for first-year students of color. In addition, he started his own community project in SLC called Roses in Glendale (Tupac influence) where marginalized students connected theories and concepts to their lived experiences; engaged in grassroots organizing for Unidad Inmigrante working with undocumented immigrants; and conducted his own research projects looking at the educational trajectories of multi-generational immigrant Latino youth using an intersectional analysis.",
        "During his free time, you can find Kevin playing soccer in the community, volunteering at his old high school, and learning how to cook his mom’s food."
      ],
      email: "mailto:kevin@urbantxt.com"
    },
    {
      id:"bagel",
      pic: "assets/pages/about/team/bagel.jpg",
      title: 'Cheif Mascot Office',
      name: "Bagel",
      bio: [
        "Bagel is a huge part of our organization and our students love him."
      ],
      email: "mailto:info@urbantxt.com"
    },
    
  ]
  public current;
  
  constructor(public activatedRouter: ActivatedRoute) { 
    this.activatedRouter.params.subscribe((params) => {
      this.current = this.team.find( (user) => {
        return user.id === params.id;
      })
    })
  }

  ngOnInit() {
  }

}
