import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private myRoute: Router) {
  }
  
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // check if current route is login page
    if (state.url === '/login'){
      if (this.auth.isLoggedIn()){
        // logged in user redirect to component page
        console.log('is logged in')
        this.myRoute.navigate(['admin/settings/manage-users']);
        return false;
      }else{
        // not logged in user
        console.log('is  not logged in')
        return true;
      }
    }  else if (this.auth.isLoggedIn()) {
        // user is logged in - let them view page
        console.log('is logged in')
        return true;
      }else{
        // user not logged in - redirect them to login page
        console.log('is  not logged in')
        this.myRoute.navigate(["login"]);
        return false;
      }
  }
}
