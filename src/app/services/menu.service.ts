import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs-compat/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private menuStatus = new BehaviorSubject<any>(false);
  public status = this.menuStatus.asObservable();

  constructor() { }

  toggleMenu() {
    this.menuStatus.next(!this.menuStatus.getValue());
  }

  setFalse() {
    this.menuStatus.next(false);
  }

  setTrue() {
    this.menuStatus.next(true);
  }
}
