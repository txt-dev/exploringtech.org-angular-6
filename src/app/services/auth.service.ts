import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public url = 'http://localhost:3000/txt-admin-2';

  constructor(private router: Router, public http: HttpClient) { }

  setToken(token: string) {
    localStorage.setItem('AdminUser', token);
    this.router.navigate(['admin/settings/manage-users'])
  }

  getToken() {
    return localStorage.getItem('AdminUser');
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  logout() {
    localStorage.removeItem('AdminUser');
    this.router.navigate(['login']);
  }

  grabUserProfile(){
    const header =   new HttpHeaders().set('authorization', this.getToken());
    return this.http.get(this.url + '/profile', { headers: header});
  }
}
