import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  // public url = 'http://localhost:3000';
  public url = "https://txt-server-back-up.herokuapp.com";

  constructor(private router: Router, public http: HttpClient) { }

  submitVolunteerForm(data) {
    return this.http.post(this.url + '/exploringtech/recordWebsiteFormVolunteer', data)
  }

  submitMentorForm(data) {
    return this.http.post(this.url + '/exploringtech/recordWebsiteFormMentor', data)
  }

  submitFellowsForm(data) {
    return this.http.post(this.url + '/exploringtech/recordWebsiteFormFellows', data)
  }

  submitContactForm(data) {
    return this.http.post(this.url + '/exploringtech/recordWebsiteFormContact', data)
  }
}
