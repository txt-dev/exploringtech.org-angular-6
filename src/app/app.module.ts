import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { BrowserTransferStateModule } from '@angular/platform-browser';

import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AppComponent } from './app.component';

// layout import
import { LayoutComponent } from './components/layout-components/layout/layout.component';
import { FooterComponent } from './components/layout-components/footer/footer.component';
import { NavComponent } from './components/layout-components/nav/nav.component';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';

// import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';

// services
import { MenuService } from './services/menu.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { FormService } from './services/form.service';
@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'tour-of-heroes' }),
    RouterModule.forRoot([
      {
        path: '', 
        component: LayoutComponent, 
        children:
          [
            {
              path: 'home',
              loadChildren: './components/home-components/home.module#HomeModule',
            },
            {
              path: 'about',
              loadChildren: './components/about-components/about.module#AboutModule',
            },
            {
              path: 'programs',
              loadChildren: './components/programs-components/programs.module#ProgramsModule',
            },
            {
              path: 'TXT-culture',
              loadChildren: './components/culture-components/culture.module#CultureModule',
            },
            {
              path: 'get-involved',
              loadChildren: './components/get-involved-components/get-involved.module#GetInvolvedModule',
            },
            {
              path: 'contact-us',
              loadChildren: './components/contact-us-components/contact-us.module#ContactUsModule',
            },
            { 
              path: '**', 
              redirectTo: 'home',
              pathMatch: 'full', 
            }
          ]
      }
    ]),
    HttpClientModule,
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    BrowserModule
  ],
  declarations: [
    AppComponent,
    LayoutComponent,
    NavComponent,
    SanitizeHtmlPipe,
    FooterComponent,
    
  ],
  providers: [MenuService, AuthGuard, AuthService, FormService],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
